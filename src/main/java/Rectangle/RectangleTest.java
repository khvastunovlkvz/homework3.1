package Rectangle;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RectangleTest extends BaseTest {

/*    public static void main(String[] args) {

        RectangleClass rectangle = new RectangleClass(4, 4);

        rectangle.getArea(rectangle.width, rectangle.height);

        rectangle.getPerimeter(rectangle.width, rectangle.height);

        rectangle.sqare(rectangle.width, rectangle.height);
   }
*/      //Проверял работоспособность методов
    RectangleClass rectangle = new RectangleClass(11,10);       //Создан объект прямоугольник

    @Test
    public void checkArea(){            //Тест площади
        int expectedResult = rectangle.width * rectangle.height;
        int actualResult = rectangle.getArea(rectangle.width, rectangle.height);
        Assert.assertEquals(actualResult, expectedResult, "Тест площади провален");
    }

    @Test
    public void checkPerimeter(){       //Тест периметра
        int expectedResult = 2 * (rectangle.width + rectangle.height);
        int actualResult = rectangle.getPerimeter(rectangle.width, rectangle.height);
        Assert.assertEquals(actualResult, expectedResult, "Тест периметра провален");
    }

    @Test
    public void checkSqare(){           //Тест фигуры
        boolean expectedResult = rectangle.height == rectangle.width;
        boolean actualResult = rectangle.sqare(rectangle.width, rectangle.height);
        Assert.assertEquals(actualResult, expectedResult, "Тест провален");
    }
}
