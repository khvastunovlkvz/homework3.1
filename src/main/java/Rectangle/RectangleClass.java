package Rectangle;

public class RectangleClass{

    public RectangleClass(){        //Пустой конструктор
        System.out.println("New Rectangle Created!");
    }

    public RectangleClass(int width, int height){       //Конструктор с инициализацией ширины и высоты
        System.out.println("Объект создан");
        this.height = height;
        this.width = width;
    }

    int width;
    int height;

    public int getArea(int width, int height){      //Метод расчет площади
//        this.width = width;
//        this.height = height;
        int area = width * height;
        System.out.println("Площадь равна : "+ area);
        return area;
    }

    public int getPerimeter(int width, int height){     //Метод расчет периметра
//        this.width = width;
//        this.height = height;
        int perimeter = 2*(width + height);
        System.out.println("Периметр равен : " + perimeter);
        return perimeter;
    }
    public boolean sqare(int width, int height){        //Метод фигура является прямоугольником или квадратом
       if (width == height){
           System.out.println("Это квадрат! ");
           return true;
       } else {
           System.out.println("Это прямоугольник! ");
           return false;
       }

    }

}
