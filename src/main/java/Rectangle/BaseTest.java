package Rectangle;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    @BeforeTest     //Предварительный тест. Выводит строку старт перед основным тестом
    public void beforeTest(){
        System.out.println();
        System.out.println("Старт ");
        System.out.println();
    }

    @AfterTest      //Пост тест. Выводит строку стоп после основного теста
    public void afterTest(){
        System.out.println();
        System.out.println("Стоп");
    }
}
